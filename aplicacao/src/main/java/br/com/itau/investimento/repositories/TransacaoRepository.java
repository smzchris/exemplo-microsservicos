package br.com.itau.investimento.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.investimento.models.Transacao;

public interface TransacaoRepository extends CrudRepository<Transacao, Integer> {

}
